<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use App\Service\ApiService;

class ApiController extends AbstractController
{
    /**
     * @var ApiService
     */
    private $apiService;

    /**
     * ApiController constructor.
     * @param ApiService $apiService
     */
    public function __construct(ApiService $apiService)
    {
        $this->apiService = $apiService;
    }

    /**
     * @Route("/api/regions", name="api_regions")
     * @param SerializerInterface $serializer
     * @return Response
     * @throws \Symfony\Component\Serializer\Exception\ExceptionInterface
     */
    public function listRegion(SerializerInterface $serializer): Response
    {
        //$regionTab = $serializer->decode($regions, 'json');
        //$regionObject = $serializer->denormalize($regionTab, 'App\Entity\Region[]');

        // Region[] signifie qu'il reçoit une liste de region
        $regionDeserialized = $serializer->deserialize($this->apiService->getRegionList(), 'App\Entity\Region[]', 'json');
        return $this->render('api/index.html.twig', [
            'regions' => $regionDeserialized
        ]);
    }

    /**
     * @Route("/api/deptregion", name="api_deptregion")
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function listDepByRegion(Request $request, SerializerInterface $serializer): Response
    {
        $codeRegion = $request->query->get('region');

        if ($codeRegion === null || $codeRegion === 'all') {
            $departments = $this->apiService->getDepartmentList();
        } else {
            $departments = $this->apiService->getDepartmentByRegion($codeRegion);
        }
        $departments = $serializer->decode($departments, 'json');
        $regionDeserialized = $serializer->deserialize($this->apiService->getRegionList(), 'App\Entity\Region[]', 'json');
        return $this->render('api/dept_region.html.twig', [
            'regions' => $regionDeserialized,
            'departments' => $departments
        ]);
    }
}
