<?php


namespace App\Service;

use Symfony\Contracts\HttpClient\HttpClientInterface;


class ApiService
{
    /**
     * @var HttpClientInterface
     */
    private $client;

    /**
     * ApiService constructor.
     * @param HttpClientInterface $client
     */
    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return string
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Exception
     */
    public function getRegionList(): string
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://geo.api.gouv.fr/regions'
            );
            if ($response->getStatusCode() === 200) {
                return $response->getContent();
            }

        } catch (\Exception $e) {
            throw new \Exception('API Error : '.  $e->getMessage());
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getDepartmentList(): string
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://geo.api.gouv.fr/departements'
            );
            if ($response->getStatusCode() === 200) {
                return $response->getContent();
            }
        } catch (\Exception $e) {
            throw new \Exception('API Error : '.  $e->getMessage());
        }
    }

    /**
     * @param string $codeRegion
     * @return string
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Exception
     */
    public function getDepartmentByRegion(string $codeRegion): string
    {
        try {
            $response = $this->client->request(
                'GET',
                'https://geo.api.gouv.fr/regions/'.$codeRegion.'/departements'
            );
            if ($response->getStatusCode() === 200) {
                return $response->getContent();
            }
        } catch (\Exception $e) {
            throw new \Exception('API Error : '.  $e->getMessage());
        }
    }
}